import static org.junit.Assert.*;

import org.junit.Test;


public class testGameOfLife {

	@Test
	public void testNeighbourCount()
	{
		GameOfLife life = new GameOfLife(10,10);
		
		life.setCellState(1,1,true);
		
		assertEquals("There should be zero neighbours",life.getNeighbourCount(1,1),0);
		
		life.setCellState(0,0,true);
		life.setCellState(1,0,true);
		
		assertEquals("There should be two neighbours",life.getNeighbourCount(1,1),2);
		
		
		
		
		
	}

}
