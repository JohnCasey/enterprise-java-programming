import javax.swing.JFrame;
import javax.swing.JPanel;

class Smiley extends JFrame {

	public Smiley() {
		super ("Smiley");
		
		JPanel drawingPanel = new SmileyPanel();
		
		getContentPane().add(drawingPanel);
		
		setSize (600, 600);
		setVisible(true);
	}

	public static void main (String[] args) {
		Smiley s = new Smiley();
		s.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
