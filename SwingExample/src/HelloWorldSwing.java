import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class HelloWorldSwing extends JFrame implements ActionListener
{

	private static final long serialVersionUID = -3787236189001805562L;

	JButton hello = new JButton("Hello World");
	
	public HelloWorldSwing()
	{
		hello.addActionListener(this);
		
		getContentPane().setLayout(new FlowLayout());
		getContentPane().add(new JLabel("Hello World"));
		getContentPane().add(hello);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setVisible(true);
		
		setSize(300,400);
	}

	@Override
	public void actionPerformed(ActionEvent event)
	{
		System.out.println("Hello World"+event);
	}

	public static void main(String[] args) {
		new HelloWorldSwing();
	}
}
