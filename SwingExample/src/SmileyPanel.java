import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;


public class SmileyPanel extends JPanel
{
	private static final long serialVersionUID = 8539973417271376384L;

	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());

		g.setColor(Color.YELLOW);
		g.fillOval(100,100,400,400);
		g.setColor(Color.BLACK);
		g.fillArc(233, 350, 132, 50, 0, -180);
		g.fillOval(200, 233, 40, 40);
		g.fillOval(360, 233, 40, 40);
		g.setColor(Color.YELLOW);
		g.fillArc(233, 340, 132, 50, 0, -180);
	}
	
}
