package books.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import books.dao.BookEntity;

@Repository
public interface BookRepository extends CrudRepository<BookEntity, Long>
{

}
