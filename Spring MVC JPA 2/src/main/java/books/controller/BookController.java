package books.controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import books.dao.BookEntity;
import books.dao.GenreEntity;
import books.model.BookGenreModel;
import books.repository.BookRepository;
import books.repository.GenreRepository;

@Controller
public class BookController
{
	private BookRepository bookRepo;
	private GenreRepository genreRepo;

	@Autowired
	public BookController(BookRepository bookRepo, GenreRepository genreRepo)
	{
		super();
		this.bookRepo = bookRepo;
		this.genreRepo = genreRepo;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/delete/{id}")
	public String delete(@PathVariable("id") Long id)
	{
		bookRepo.delete(id);
		
		return "redirect:/books";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/books")
	public String home(Map<String, Object> model)
	{
		Iterable<BookEntity> books = bookRepo.findAll();
		model.put("books", books);
		return "home"; // name of the view
	}

	@RequestMapping(method = RequestMethod.POST, value = "/books")
	public String save(BookGenreModel bookModel)
	{
		// convert from BookGenreModel -> BookEntity + GenreEntity
		
		BookEntity bookEntity = new BookEntity();
		bookEntity.setTitle(bookModel.getTitle());
		bookEntity.setAuthor(bookModel.getAuthor());
		
		String genres = bookModel.getGenres();
		
		List<GenreEntity> genreEntities = new LinkedList<GenreEntity> ();
		
		String[] list = genres.split(",");
		for(int i = 0; i< list.length ;i++)
		{
			GenreEntity genreEntity = new GenreEntity();
			genreEntity.setName(list[i]);
			genreEntity.setDescription(list[i]);
			
			genreEntity = genreRepo.save(genreEntity);
			
			genreEntities.add(genreEntity);
		}
				
		bookEntity.setGenres(genreEntities);
		
		bookEntity = bookRepo.save(bookEntity);

		return "redirect:/books";
	}
}
